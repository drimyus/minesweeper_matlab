close all
clear
clc
 
% field_width = 8;
% field_height = 8;
% number_of_mines = 10;
 
field_width = 16;
field_height = 16;
number_of_mines = 30;
 
% field_width = 30;
% field_height = 16;
% number_of_mines = 99;
 
% ========================================================================
 
minesweeper(field_width, field_height, number_of_mines);