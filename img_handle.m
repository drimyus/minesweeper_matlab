classdef img_handle < hgsetget
    properties
        img_0
        img_1
        img_2
        img_3
        img_4
        img_5
        img_6
        img_7
        img_8
        click
        mine
        uncvr
        boom
        flag
        miss_flag
        win_mine
        state_happy
        state_click
        state_win
        state_dead
    end
     
    methods
        function obj = img_handle()
            obj.img_0 = imread('imgs/0.png');
            obj.img_1 = imread('imgs/1.png');
            obj.img_2 = imread('imgs/2.png');
            obj.img_3 = imread('imgs/3.png');
            obj.img_4 = imread('imgs/4.png');
            obj.img_5 = imread('imgs/5.png');
            obj.img_6 = imread('imgs/6.png');
            obj.img_7 = imread('imgs/7.png');
            obj.img_8 = imread('imgs/8.png');
            obj.click = imread('imgs/click.png');
            obj.mine  = imread('imgs/mine.png');
            obj.uncvr = imread('imgs/uncovered.png');
            obj.boom  = imread('imgs/boom.png');
            obj.flag  = imread('imgs/flag.png');
            obj.miss_flag = imread('imgs/miss_flag.png');
            obj.win_mine = imread('imgs/win_mine.png');
            obj.state_happy = imread('imgs/state_happy.png');
            obj.state_click = imread('imgs/state_click.png');
            obj.state_win = imread('imgs/state_win.png');
            obj.state_dead  = imread('imgs/state_dead.png');
        end
    end
end